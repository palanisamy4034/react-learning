import * as io from 'socket.io-client';
class SocketService {
    private endpoint = "http://127.0.0.1:4000"
    private socket: any;
    public enableIOConnection(user: any) {
        this.socket = io(this.endpoint);
        this.socket.on('connect', () => {
            if (user === undefined) {
                user = localStorage.getItem('userData');
            }
            this.socket.emit('join', user);
        })
        return this.socket;
    }
    public getIOConnection() {
        return this.socket;
    }

    public reiveMessage() {
        this.socket.on('messagerecive', (response: any) => {
            // tslint:disable-next-line:no-console
            console.log(response)
        });
    }

}

export default new SocketService;