import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {Provider} from 'react-redux';
import store from './store/Store';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faIgloo } from '@fortawesome/free-solid-svg-icons'
library.add(faIgloo)

ReactDOM.render(
    <Provider store={store}>
          <App />    
    </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
