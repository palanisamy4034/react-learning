class ApiConfig {
  public userInfo={
    GET_USER:'user',
    USER_LOGIN:'auth/login',
    REGISTER_USER:'auth/signup',
    UPDATE_USER:'user/update',
  }

  public chatConv = { 
    CHAT_TRANSSCRIPT :'chat',
    contacts:'user/contacts',
    createConv:'chat/createConvId',
    DELETE_CHAT:'chat/delete',
  }

}
export default new ApiConfig
