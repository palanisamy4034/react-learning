import axios from 'axios';
import { toast } from 'react-toastify';

class Service {
  public service: any;
  public requestHeader: any;
  constructor() {
    const service = axios.create({
      baseURL: `http://localhost:4000`,
    });
    service.interceptors.response.use(this.handleSuccess, this.handleError);
    this.service = service;
    const authToken = localStorage.getItem('authToken');
    const userId = localStorage.getItem('userData');
    this.setHeaders({ authToken, userId });
  }

  public handleSuccess(response: any) {
    return Promise.resolve(response);
  }

  public handleError = (error: any) => {
    if (error !== undefined) {
      if (error.response) {
        const { msg } = error.response.data;
        const toastMsg = msg !== undefined ? msg : error.message;
        toast.error(toastMsg);
      }
      else {
        return Promise.reject(error)
      }
    }
    switch (error.response.status) {
      case 401:
        localStorage.clear();
        this.redirectTo(document, '/login')
        break;
      // case 404:
      //   this.redirectTo(document, '/404')
      //   break;
      // default:
      //   this.redirectTo(document, '/signup')
      //   break;
    }
    return Promise.reject(error)
  }

  public redirectTo = (document: any, path: any) => {
    document.location = path
  }

  public get(path: any, query = '') {
    if (query !== null && query !== undefined) {
      path = `${path}/${query}`;
    }
    return this.service.get(path).then(
      (response: any) => {
        return response.data;
      });
  }

  public patch(path: any, payload: any) {
    return this.service.request({
      method: 'PATCH',
      url: path,
      responseType: 'json',
      data: payload
    }).then((response: any) => response);
  }

  public post(path: any, payload: any) {
    return this.service.request({
      method: 'POST',
      url: path,
      responseType: 'json',
      data: payload
    }).then((response: any) => response.data);
  }

  public setHeaders(response: any) {
    this.service.defaults.headers.common['x-client-id'] = response.userId;
    this.service.defaults.headers.common.Authorization = response.authToken;
    this.service.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  }
}

export default new Service;
