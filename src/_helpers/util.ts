export const convertTimestampToHM = (timestamp: any) => {
    let h: any = new Date(timestamp).getHours();
    let m: any = new Date(timestamp).getMinutes();
    h = (h < 10) ? '0' + h : h;
    m = (m < 10) ? '0' + m : m;
    let ampm = 'AM'
    if (h > 12) {
        h = h - 12;
        h = (h < 10) ? '0' + h : h;
        ampm = ' PM';
    }
    return `${h}:${m} ${ampm}`;
};

export const sortBylastMessage = (a: any, b: any) => {
    return new Date(b.messages[b.messages.length - 1].eventTime) >
        new Date(a.messages[a.messages.length - 1].eventTime) ? 1 : -1;
}

export const compareDate = (previous: any, current: any) => {
    previous = new Date(previous);
    current = new Date(current);
    const dataFilter = (current.toDateString()).split(" ");
    dataFilter.pop();
    const dateString = dataFilter.join(" ")
    if (previous.toDateString().localeCompare(current.toDateString()) === 0) {
        return { isValid: false, spliterDate: dateString};
    }
    return { isValid: true, spliterDate: dateString };
}
