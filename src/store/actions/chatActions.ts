import {chatConstants } from '../constants/user-constants';

export const globalcontacts = (data:any) => (dispatch: any) => dispatch({
    type: chatConstants.FETCH_CONTACTS,
    payload: data
});

export const activeChat = (data:any) => (dispatch: any) => dispatch({
    type: chatConstants.ACTIVE_CONV,
    payload: data
});

export const updateCoversationMessages = (data:any) => (dispatch: any) => dispatch({
    type: chatConstants.UPDATE_CONV_MSG,
    payload: data
});

export const initCoversationMessages = (data:any) => (dispatch: any) => dispatch({
    type: chatConstants.GET_INIT_CONV_MSG,
    payload: data
});

export const deleteMessages = (data:any) => (dispatch: any) => dispatch({
    type: chatConstants.DEL_CONV_MSG,
    payload: data
});