import * as actions from './actionTypes';
import Api from './../../config/Api';
import { userConstants} from '../constants/user-constants';


export const fetchPosts = () => (dispatch: any) => {
  Api.get('posts')
    .then((res: any) => dispatch(fetchPostSuccess(res)))
    .catch((res: any) => dispatch(fetchPostError(res)));
}
export const loginInfo = (data: any) => (dispatch: any) => dispatch({
  type: userConstants.LOGIN_SUCCESS,
  payload: data
});

export const fetchPostSuccess = (posts: any) => ({
  type: actions.FETCH_POSTS,
  payload: { data: posts, error: '' }
});

export const fetchPostError = (err: any) => ({
  type: actions.FETCH_POSTS,
  payload: { data: [], error: err }
});

