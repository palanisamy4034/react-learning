
export interface InterChatConvModel {
    roomId: string;
    recipient: [];
    messages: any[];
    unReadChat: number;
}