import { loaderConstants } from '../constants/user-constants';

const initialstate = {
  loaderIsOpen: false,
  loaderType: {
    message: '',
    type: 1
  },
  popupModal: {
    message: '',
    type: 1
  },
  popupIsOpen: false
}


export default ((state: any = initialstate, action: any) => {
  switch (action.type) {
    case loaderConstants.LOADING_INFO:
      return Initialloading(state, action)
    case loaderConstants.POPUP_MODAL_INFO:
      return InitialPOPUP(state, action)
    default:
      return state
  }
});


export const Initialloading = (state: any, action: any) => {
  const { payload } = action;
  return { ...state, loaderIsOpen: payload.isOpen, loaderType: payload.loaderType };
};

export const InitialPOPUP = (state: any, action: any) => {
  const { payload } = action;
  return { ...state, popupIsOpen: payload.isOpen, popupModal: payload.loaderType };
};