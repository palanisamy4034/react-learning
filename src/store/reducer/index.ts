import {combineReducers }from 'redux';
import postReducer from './PostReducers';
import loginReducer from './loginReducer';
import chatReducer from './chatReducer';
import loaderReducer from './loaderReducer';


 export default combineReducers({
   postsData:postReducer,
   loginData:loginReducer,
   chatData:chatReducer,
   loaderStatus:loaderReducer
 });
