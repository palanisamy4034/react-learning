import * as actions from './../actions/actionTypes';
import { toast } from "react-toastify";


const initialstate = {
  posts: [],
  Error: [],
  loginInfo: []
}


export default ((state: any = initialstate, action: any) => {
  switch (action.type) {
    case actions.FETCH_POSTS:
      return updatePostsData(state, action);
    default:
      return state;
  }
})

export const updatePostsData = (state: any, action: any) => {
  if (action.payload.error!== '') {
    toast.error(action.payload.error.message);
    return { ...state, posts: state.posts, Error: [...state.Error, ...action.payload.error] };
  }
  // toast.success("INCREMENT");
  return { ...state, posts: action.payload.data, Error: [...state.Error] };
};
