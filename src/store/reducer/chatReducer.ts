import { chatConstants } from '../constants/user-constants';
import * as _ from 'lodash';
import * as util from 'src/_helpers/util';
import { differenceWith, isEqual } from 'lodash';

const initialstate = {
  contacts: [],
  Error: [],
  conversations: [],
  activeConv: {
    roomId: '',
    recipient: '',
    messages: [],
    unReadChat: 0
  },
  newMsg: true
}

export default ((state: any = initialstate, action: any) => {
  switch (action.type) {
    case chatConstants.FETCH_CONTACTS:
      return { ...state, contacts: action.payload };
    case chatConstants.ACTIVE_CONV:
      return updateCurrentUser(state, action);
    case chatConstants.UPDATE_CONV_MSG:
      return updateconversation(state, action);
    case chatConstants.GET_INIT_CONV_MSG:
      return Initialconversation(state, action);
    case chatConstants.DEL_CONV_MSG:
      return deleteConversationMessage(state, action);
    default:
      return state;
  }
});

export const updateCurrentUser = (state: any, action: any) => {
  const payload = action.payload;
  const tempState = _.cloneDeep(state);
  const tempConv = tempState.conversations.map((e: any) => {
    if (e.roomId !== payload.roomId) {
      e.isActive = false;
    } else {
      e.isActive = true;
    }
    return e;
  });
  return { ...state, conversations: tempConv, activeConv: payload };
};

export const updateconversation = (state: any, action: any) => {
  const payload = action.payload;
  const tempState = _.cloneDeep(state);
  const { conversations, activeConv } = tempState
  const { roomId } = payload;
  const idx = _.findIndex(conversations, { "roomId": roomId })
  if (idx !== -1) {
    conversations[idx].messages.push(payload);
    conversations[idx].unReadChat = conversations[idx].unReadChat + 1;

    if (activeConv.roomId !== payload.roomId) {
      conversations[idx].unReadChat = conversations[idx].unReadChat + 1;
    } else {
      conversations[idx].unReadChat = 0;
    }
  } else if (payload !== undefined) {
    const newChat = { ...tempState.activeConv, ...{ messages: [payload] } };
    conversations.push(newChat);
  }
  return { ...state, conversations };
};

export const Initialconversation = (state: any, action: any) => {
  let payload = action.payload;
  payload = payload.sort(util.sortBylastMessage);
  return { ...state, "conversations": payload, activeConv: payload[0] };
};

export const deleteConversationMessage = (state: any, action: any) => {
  const payload = action.payload;
  const tempState = _.cloneDeep(state);
  const { conversations} = tempState
  const { roomId } = payload;
  const idx = _.findIndex(conversations, { "roomId": roomId })
  if (idx !== -1) {
    conversations[idx].messages = (deleleteById(conversations[idx].messages, [{ _id: payload._id }]));
  }
  return { ...state, conversations };
};


export const deleleteById = (data: any[], key: any[]) => {
  key.forEach((ele) => {
    const tempObj = data.filter((e: any) => {
      let isValid = true;
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < Object.keys(ele).length; i++) {
        if (e.hasOwnProperty(Object.keys(ele)[i]) && ele.hasOwnProperty(Object.keys(ele)[i])) {
          if (e[Object.keys(ele)[i]] !== ele[Object.keys(ele)[i]]) {
            isValid = false;
            break;
          }
        }
      }
      return isValid;
    })
    data = differenceWith(data, tempObj, isEqual);
  });
  return data;
}