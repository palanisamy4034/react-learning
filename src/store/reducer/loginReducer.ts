import { userConstants } from '../constants/user-constants';


const initialstate = {
  item: {},
  loginInfo:[],
  isLoggedIn: false
}


export default ((state: any = initialstate, action: any) => {
  switch (action.type) {
    case userConstants.LOGIN_SUCCESS:
      return InitialLogin(state,action)
    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        items: action.users
      };
    case userConstants.GETALL_FAILURE:
      return { 
        error: action.error
      };
    case userConstants.DELETE_REQUEST:
    default:
      return state
  }
});


export const InitialLogin = (state: any, action: any) => {
  const payload = action.payload;
  if(payload!==undefined){
  return { ...state, loginInfo: payload, isLoggedIn: true };
  }
  return { ...state, loginInfo: payload, isLoggedIn: false };
};