import * as React from 'react';
import { connect } from 'react-redux';
import API from 'src/config/Api';
import apiUrl from 'src/config/api.url';
import { Redirect } from 'react-router';
import { loginInfo } from 'src/store/actions/PostActions';
import { enableLoader } from 'src/store/actions/loaderActions';

class Authentication extends React.Component<any, any>{
    public async componentWillMount() {
        await this.redirectto();
    }
    public redirectto() {
        const cachedHits: any = localStorage.getItem('userData');
        if (cachedHits === undefined || cachedHits === null || cachedHits === '') {
            const path = window.location.pathname;
            if(path === '/signup'){
                return (<Redirect to='/signup' />)
            }else if (path !== '/login') {
                return (<Redirect to='/login' />)
            }
        } else {
            this.props.enableLoader({isOpen:true});
            API.get(apiUrl.userInfo.GET_USER, cachedHits).then((data: any) => {
                this.props.loginInfo(data);
                this.props.history.push('/chat')
            });
        }
        return
    }
    public render() {
        const isLoggedIn: boolean = this.props.user.isLoggedIn;
        const existingLoggedIn = () => {
            const cachedHits: any = localStorage.getItem('userData');
            if (cachedHits === undefined || cachedHits === null || cachedHits === '') {
                    return (<Redirect to='/login' />)
                }
                return (<div>Loading...</div>)
            }

        return (<div>
            {isLoggedIn === false && existingLoggedIn()}
        </div>);
    }
}
const mapStateToProps = (state: any) => ({
    user: state.loginData
})

export default connect(mapStateToProps, { loginInfo, enableLoader })(Authentication);