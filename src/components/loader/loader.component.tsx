import * as React from 'react';
import './loader.component.scss'

class LoaderComponent extends React.Component<any, any>{

    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title }
    }

    public render() {
        return (<section className="global-loader">
            <div className="loading">
            <img src="images/loading.svg" alt="" width="200"/>
            </div>
        </section>);
    }
}

export default LoaderComponent;
