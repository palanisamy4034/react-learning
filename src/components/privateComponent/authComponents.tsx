import * as React from 'react';
import SideBar from '../admin/Sidebar/Sidebar';
import RouteComponent from '../admin/routecomponent/routeComponent';
import { connect } from 'react-redux';
import { loginInfo } from 'src/store/actions/PostActions';
import SocketService from './../../socket/socketConnection'
import store from 'src/store/Store';


class AuthComponent extends React.Component<any, any>{


    constructor(props: any) {
        super(props);
    }
    public select(state: any) {
        return state.some.deep.property
    }
    public unsubscribe = () => store.subscribe(this.handleChange)
    public handleChange() {
        if (this.props.user.isLoggedIn === true) {
            SocketService.enableIOConnection(this.props.user);
        }
    }
    public render() {
        return (<div>
            <div className="sidebar">
                <SideBar />
            </div>
            <div className="container-component">
                <RouteComponent />
            </div>
        </div>)
    }
}
const mapStateToProps = (state: any) => ({
    user: state.loginData
})

export default connect(mapStateToProps, { loginInfo })(AuthComponent);