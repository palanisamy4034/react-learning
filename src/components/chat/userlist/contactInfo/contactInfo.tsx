import * as React from 'react';
import { connect } from 'react-redux';
import apiUrl from 'src/config/api.url';
import API from 'src/config/Api';
import * as PropTypes from 'prop-types'
import { globalcontacts, activeChat } from '../../../../store/actions/chatActions';
import { cloneDeep } from 'lodash';

class ContactInfo extends React.Component<any, any>{
    public static propTypes = {
        searchValue: PropTypes.any.isRequired,
    }
    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title, contacts: [] }
        this.getUserList();
    }
    public getUserList() {
        const userId: any = localStorage.getItem('userData');
        API.get(apiUrl.chatConv.contacts, userId).then((data: any) => {
            this.props.globalcontacts(data);
        });
    }
    public handleClick(eve: any) {
        if (this.props.user.loginInfo.userId !== null) {
            const recipient = { recipient: [eve.userId, this.props.user.loginInfo.userId], roomType: 'direct' }
            API.post(apiUrl.chatConv.createConv, recipient).then((obj: any) => {
                this.props.activeChat(obj);
                this.props.tabSwitch(1);
            });
        }
    }
    public render() {

        let contactList = [`<li/>`];
        if (this.props.contacts !== undefined && this.props.contacts.length > 0) {
            let tempContact = cloneDeep(this.props.contacts)
            if (this.props.searchValue !== undefined && this.props.searchValue !== '') {
                tempContact = tempContact.filter((e: any) => {
                    return (e.userName).toLowerCase().includes(this.props.searchValue.toLowerCase());
                });
            }
            contactList = tempContact.map((e: any, i: any) => {
                return (<li className="person" key={i} onClick={this.handleClick.bind(this, e)} >
                    <img src="/images/user.svg" alt="" />
                    <div className="name">{e.userName}</div>
                    <span className="preview">I was wondering...</span>
                </li>)
            });
        }
        return (<ul className="people">{contactList}</ul>)
    };
}
const mapStateToProps = (state: any) => ({
    user: state.loginData,
    contacts: state.chatData.contacts
})
export default connect(mapStateToProps, { globalcontacts, activeChat })(ContactInfo);