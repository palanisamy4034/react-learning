import * as React from 'react';
import { connect } from 'react-redux';
import { globalcontacts, activeChat } from '../../../../store/actions/chatActions';
import * as _ from 'lodash'
import * as util from 'src/_helpers/util';
import { cloneDeep } from 'lodash';

class ConvDetails extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title, contacts: [] }
    }
    public handleClick(eve: any) {
        const conv = cloneDeep(eve);
        delete conv.messages
        this.props.activeChat(eve);
    }
    public render() {
        const getuserName = (ele: any) => {
            const idx = ele.recipient.indexOf(this.props.user.loginInfo.userId) === 0 ? 1 : 0;
            const { userName } = _.filter(this.props.contacts, (o) => o.userId === ele.recipient[idx])[0];
            return userName;
        }
        const convertTimestampToHM = (ele: any) => {
            return util.convertTimestampToHM(ele.messages[ele.messages.length - 1].eventTime)
        }

        let chatHistory = [``];
        if (this.props.conversations !== undefined && this.props.conversations.length > 0) {
            let conversations = cloneDeep(this.props.conversations);
            if (this.props.searchValue !== undefined && this.props.searchValue !== '') {
                conversations = conversations.filter((e: any) => {
                    return getuserName(e).toLowerCase().includes(this.props.searchValue.toLowerCase());
                });
            }
            conversations = conversations.sort(util.sortBylastMessage);
            chatHistory = conversations.map((e: any, i: any) => {
                const { messages } = e;
                if (messages !== undefined && messages.length > 0) {
                    return (<li className={e.roomId === this.props.activeConv.roomId ? 'person active' : 'person'} key={e.roomId} onClick={this.handleClick.bind(this, e)} >
                        <img src="/images/user.svg" alt="" />
                        <div className="name">{getuserName(e)}</div>
                        <span className="time">{convertTimestampToHM(e)}</span>
                        <span className="preview">{e.messages[e.messages.length - 1].payload}</span>
                    </li>)
                }
                return;
            });
        }
        return (<ul className="people">{chatHistory}</ul>)
    };
}

const mapStateToProps = (state: any) => ({
    user: state.loginData,
    contacts: state.chatData.contacts,
    conversations: state.chatData.conversations,
    activeConv: state.chatData.activeConv,
})
export default connect(mapStateToProps, { globalcontacts, activeChat })(ConvDetails);