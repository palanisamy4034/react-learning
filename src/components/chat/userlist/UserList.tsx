import * as React from 'react';
import './userList.scss';
import ConvDetails from './convDetail/convDetail';
import ContactInfo from './contactInfo/contactInfo';
import { connect } from 'react-redux';
import { Tabs, Tab } from 'react-bootstrap';
// import {FaUsers} from 'react-icons/fa';

class UserLists extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.handleSelect = this.handleSelect.bind(this);
        this.state = { key: 1, searchValue: '' ,convDetailSearch:'',contactSearch:''};
        this.handleChange = this.handleChange.bind(this);
    }

    public handleChange(event: any) {
        this.setState({ searchValue: event.target.value });
        if (this.state.key === 1) {
            this.setState({ convDetailSearch: event.target.value });
        } else {
            this.setState({ contactSearch: event.target.value });
        }
    }
    
    public handleSelect(key: any) {
        if (key === 1) {
            this.setState({ key: 1, searchValue: this.state.convDetailSearch });
        } else {
            this.setState({ key, searchValue: this.state.contactSearch });
        }
    }

    public render() {
        return (<div className="inbox_people">
            <div className="top">
                <input className="search-box" type="text" required={true} value={this.state.searchValue} onChange={this.handleChange} placeholder="Search" />
                {/* <button className="close-icon" type="reset"/> */}
            </div>
            <Tabs
                animation={false}
                activeKey={this.state.key}
                onSelect={this.handleSelect}
                id="controlled-tab-example">
                <Tab eventKey={1} title={<img src="/images/mail.svg" width="36" />}>
                    <ConvDetails searchValue={this.state.convDetailSearch} />
                </Tab>
                <Tab eventKey={2} title={<img src="/images/phone-book.svg" width="36" />}>
                    <ContactInfo searchValue={this.state.contactSearch}  tabSwitch={this.handleSelect}/>
                </Tab>
            </Tabs>
        </div>)
    };

}
const mapStateToProps = (state: any) => ({
    user: state.loginData,
    contacts: state.chatData.contacts
});

export default connect(mapStateToProps)(UserLists);