import * as React from 'react';
import ChatInput from './chatInput/chatInput';
import ChatHistory from './chatHistory/chatHistory';


class MessageData extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title }
    }
    public render() {
        return (<div className="conv_area">
            <ChatHistory />
            <ChatInput />
        </div>)
    };

}

export default MessageData;