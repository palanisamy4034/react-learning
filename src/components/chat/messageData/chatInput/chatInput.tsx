import * as React from 'react';
import SocketService from '../../../../socket/socketConnection'
import { connect } from 'react-redux';
// import {filter} from 'lodash'


class ChatInput extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = {
            title: this.props.title, value: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEnterPress = this.onEnterPress.bind(this);
    }

    public handleChange(event: any) {
        this.setState({ value: event.target.value });
    }
    public onEnterPress = (event: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (event.keyCode === 13 && event.shiftKey === false) {
            this.handleSubmit(event);
        }
    }
    public handleSubmit(event: any) {
        event.preventDefault();
        if (this.state.value !== '' || this.state.value === undefined) {
            const payload = this.constructPayload(this.state.value);
            const io = SocketService.getIOConnection()
            io.emit('message', payload);
            this.setState({ value: '' })
        }
    }
    public constructPayload(msg: any) {
        const activeRoomId = this.props.currentConv;
        const senderDtl = this.props.user.loginInfo
        return {
            roomId: activeRoomId.roomId,
            msgType: 'direct',
            payload: msg,
            mediaPath: String,
            status: 'send',
            direction: 'direct',
            reference: '',
            scheduledAt: '',
            sender: {
                userId: senderDtl.userId,
                displayName: senderDtl.userName,
                avatarUrl: ''
            }
        }
    }

    public render() {
        return (
            <div className="write">
                <form onSubmit={this.handleSubmit}>
                    <div className="input_msg_write">
                        <a href="javascript:;" className="write-link attach" />
                        <textarea className="write_msg" value={this.state.value} onKeyDown={this.onEnterPress} onChange={this.handleChange} rows={1} />
                        <a href="javascript:;" className="write-link smiley" />
                        <button className="msg_send_btn" type="submit">
                            <img src="images/send-message-button.svg" />
                        </button>
                    </div>
                </form>
            </div>)
    };

}

const mapStateToProps = (state: any) => ({
    user: state.loginData,
    currentConv: state.chatData.activeConv,
    conversations: state.chatData.conversations
})
export default connect(mapStateToProps)(ChatInput);