

import * as React from 'react';
import { MenuItem } from "react-contextmenu";
import { MdDelete } from 'react-icons/md';
import { FaInfo } from 'react-icons/fa';
import { connect } from 'react-redux';
import { enableLoader } from 'src/store/actions/loaderActions';
import { deleteMessages } from 'src/store/actions/chatActions';
import apiUrl from 'src/config/api.url';
import API from 'src/config/Api';

// tslint:disable-next-line:no-unused-expression
class MessageContextItems extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    public handleClick = (id: any) => {
        if (id === 'delete') {
            const deleteMessage = {
                "_id": this.props.message._id,
                "roomId": this.props.message.roomId,
                "userId": this.props.user.userId
            }
            API.post(apiUrl.chatConv.DELETE_CHAT, deleteMessage).then((obj: any) => {
                this.props.deleteMessages(obj)
            });
        }
    }

    public render() {
        return (<div>
            <MenuItem onClick={this.handleClick.bind(this, 'delete')}>
                <MdDelete /> delete
              </MenuItem>
              <MenuItem onClick={this.handleClick.bind(this, 'details')}>
                <FaInfo /> details
            </MenuItem>
        </div>)
    }
}

const mapStateToProps = (state: any) => ({
    user: state.loginData.loginInfo,
    currentuser: state.chatData.activeConv,
    conversations: state.chatData.conversations,
    contacts: state.chatData.contacts
})

export default connect(mapStateToProps, { enableLoader, deleteMessages })(MessageContextItems);