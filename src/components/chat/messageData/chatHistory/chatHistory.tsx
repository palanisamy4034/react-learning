import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash'
import API from 'src/config/Api';
import apiUrl from 'src/config/api.url';
import { initCoversationMessages } from 'src/store/actions/chatActions';
import { cloneDeep } from 'lodash';
import { enableLoader } from 'src/store/actions/loaderActions';
import * as util from 'src/_helpers/util';
import MessageItems from './message';


class ChatHistory extends React.Component<any, any>{
  public messagesEnd: any;
  constructor(props: any) {
    super(props);
    this.state = { title: this.props.title }
    this.getConversations();
    this.handleClick = this.handleClick.bind(this)
  }
  public scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  public componentDidMount() {
    this.scrollToBottom();
  }

  public componentDidUpdate() {
    this.scrollToBottom();
  }

  public getConversations() {
    API.get(apiUrl.chatConv.CHAT_TRANSSCRIPT).then((chats: any) => {
      this.props.initCoversationMessages(chats);
      this.props.enableLoader({ isOpen: false });
    });
  }

  public handleClick=(event:any)=> {
  // tslint:disable-next-line:no-console
  console.log(event);
  }

  public render() {
    let previousDate = new Date();

    const msgSpliter = (ele: any) => {
      const { isValid, spliterDate } = util.compareDate(previousDate, ele.eventTime);
      previousDate = ele.eventTime;
      if (isValid) {
        return (<div className="d-flex justify-content-center p-2 date-spliter">
          <div className="content">
            {spliterDate}
          </div>
        </div>)
      }
      return (<div />)
    }

    const getCurrentConvMessage = () => {
      const { roomId } = this.props.currentuser;
      if (roomId !== undefined) {
        const idx = _.findIndex(this.props.conversations, { "roomId": roomId });
        if (idx !== -1) {
          const Tempconv = cloneDeep(this.props.conversations[idx])
          const { messages } = Tempconv;
          if (messages !== undefined) {
            return messages.map((msg: any, i: any) => {
              return (<div key={i}>
                {msgSpliter(msg)}
                <MessageItems userInfo={this.props.user.loginInfo} message={msg} onClick={this.handleClick} />
              </div>)
            });
          }
        }
        return (<div>no message</div>)
      } else {
        return (<div>no message</div>)
      }
    };

    return (<div className="msg_history">
      <div className="chat">
        {getCurrentConvMessage()}
        <div style={{ float: "left", clear: "both" }}
          ref={(el) => { this.messagesEnd = el; }} />
      </div>
    </div>)
  };
}
const mapStateToProps = (state: any) => ({
  user: state.loginData,
  currentuser: state.chatData.activeConv,
  conversations: state.chatData.conversations,
  contacts: state.chatData.contacts
})

export default connect(mapStateToProps, { enableLoader, initCoversationMessages })(ChatHistory);