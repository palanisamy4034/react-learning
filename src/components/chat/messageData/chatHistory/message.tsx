


import * as React from 'react';
import * as util from 'src/_helpers/util';
import { ContextMenu,  ContextMenuTrigger } from "react-contextmenu";
import * as PropTypes from 'prop-types'
import MessageContextItems from './messageHistoryContextMenu'

// tslint:disable-next-line:no-unused-expression
class MessageItems extends React.Component <any, any>{
  public messagesEnd: any;
  public  propTypes: { message: PropTypes.Validator<object>; userInfo: PropTypes.Validator<object>; handleClick: PropTypes.Requireable<(...args: any[]) => any>; };
  constructor(props: any) {
    super(props);
  }

  public render() {

    const convertTimestampToHM = (ele: any) => {
      return util.convertTimestampToHM(ele.eventTime)
    }

    const appendMsg = (data: any) => {
      const { userId } = this.props.userInfo;
      const sender = userId !== data.sender.userId ? true : false;
      return (
        <div>
          <ContextMenuTrigger id={data._id}>
            <div>
              <div className={sender ? 'd-flex align-items-end' : 'd-flex align-items-end flex-row-reverse'}>
                <div >
                  <img className="you-img" width="36" src="https://image.flaticon.com/icons/svg/145/145852.svg" />
                </div>
                <div className={sender ? 'bubble you' : 'bubble me'}>
                  <div className="msg-headerinfo d-flex justify-content-between">
                    <div>{data.sender.displayName}</div>
                    <div>{convertTimestampToHM(data)}</div>
                  </div>
                  <div>
                    {data.payload}
                  </div>
                </div>
              </div>
            </div>
          </ContextMenuTrigger>
          <ContextMenu id={data._id}>
            <MessageContextItems message={data} roomId=""/>
          </ContextMenu></div>);
    };

    return (<aside>
      {appendMsg(this.props.message)}
    </aside>
    )

  }
}
// MessageItems.propTypes = {
//   message: PropTypes.object.isRequired,
//   userInfo: PropTypes.object.isRequired,
//   handleClick: PropTypes.func,
// };


export default (MessageItems);
