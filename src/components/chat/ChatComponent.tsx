import * as React from 'react';
import UserLists from './userlist/UserList';
import MessageData from './messageData/MessageData';


class ChatComponent extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title }
    }
    public render() {
        return (<div>
            <UserLists/>
            <MessageData/>
    </div>)
    };
}

export default ChatComponent;