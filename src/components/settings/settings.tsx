import * as React from 'react';
import './settings.scss'
import UserInfoDetails from './userInfoDetails/userInfoDetails'
import { Card, CardBody, CardTitle, Col, Row } from 'reactstrap';

class Settings extends React.Component<any, any>{


  constructor(props: any) {
    super(props);
    this.state = { title: this.props.title }
  }
  public render() {
    // return (<div>{this.props.title}</div>);
    return (<section id="">
      <div className="container settings-container">
        <Row>
          <Col sm="4">
          <div className="my-5">
            <Card body={true}>
              <CardBody>
                <CardTitle className="align-right">User Info</CardTitle>
                <UserInfoDetails isEditing={true}/>
              </CardBody>
            </Card>
            </div>
          </Col>
        </Row>
      </div>
    </section>);
  }
}

export default Settings;
