import * as React from 'react';
import { FormGroup, Input, InputGroup, InputGroupAddon, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { loginInfo } from '../../../store/actions/PostActions'
import Api from '../../../config/Api';
import API_URL from '../../../config/api.url';

import '../settings'

class UserInfoDetails extends React.Component<any, any>{
  public persons: any;

  constructor(props: any) {
    super(props);

    this.state = {
      username: this.props.user.userName,
      mobile: this.props.user.userMobile,
      isEditing: false
    };
    this.handleEditing = this.handleEditing.bind(this)
  }
  // tslint:disable-next-line:no-empty
  public componentWillMount() {

  }
  public handleEditing() {
    this.setState({ isEditing: !this.state.isEditing });
  }
  
  public validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  public handleChange = (event: any) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  public handleSubmit = (event: any) => {
    const users = {
      userName: this.state.username,
      userMobile: this.state.mobile,
      userId:this.props.user.userId
    }
    Api.post(API_URL.userInfo.UPDATE_USER, users).then((data: any) => {
      this.props.loginInfo(data);
      this.handleEditing()
    })
    event.preventDefault();
  }

  public render() {
    const isEditing = this.state.isEditing;
    if (!isEditing) {
      return (
        <div>
          <div className="my-5">
            <div>UserName : {this.props.user.userName}</div>
            <div>Mobile : {this.props.user.userMobile}</div>
          </div>
          <div>
            <Button color="primary" size="lg" onClick={this.handleEditing} active={true}>EDIT</Button>
          </div>
        </div>);

    }
    return (<form>
      <div className="my-5">
        <FormGroup >
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <img width="36" height="18" src="/images/user.svg" alt="" />
            </InputGroupAddon>
            <Input id="username"
              autoFocus={true}
              type="text"
              value={this.state.username}
              onChange={this.handleChange}
              placeholder="User Name" />
          </InputGroup>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <img width="36" height="18" src="/images/mobile.svg" alt="" />
            </InputGroupAddon>
            <Input id="mobile"
              autoFocus={true}
              type="text"
              value={this.state.mobile}
              onChange={this.handleChange}
              placeholder="Mobile" />
          </InputGroup>
        </FormGroup>
      </div>
      <div>
        <Button color="primary" size="lg" onClick={this.handleSubmit} active={true}>Update</Button>
        <Button color="secondary" size="lg" onClick={this.handleEditing} active={true}>Cancel</Button>
      </div>      {/* <div className="signup-btn"  >Sign UP</div> */}
    </form>);
  }
}

const mapStateToProps = (state: any) => ({
  user: state.loginData.loginInfo
})
export default connect(mapStateToProps, { loginInfo })(UserInfoDetails);

