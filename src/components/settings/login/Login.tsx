import * as React from 'react';
import { FormGroup, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { connect } from 'react-redux';
import { loginInfo } from './../../../store/actions/PostActions'

import '../settings'
import Api from 'src/config/Api';
import API_URL from 'src/config/api.url';

class Login extends React.Component<any, any>{
  public persons: any;

  constructor(props: any) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  // tslint:disable-next-line:no-empty
  public componentWillMount() {
    const cachedHits = localStorage.getItem('userData');
    if (cachedHits) {
      // this.setState({ hits: JSON.parse(cachedHits) });
      return;
    }
  }

  public validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  public handleChange = (event: any) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  public handleSubmit = (event: any) => {
    const users = {
      userEmail: this.state.email,
      userPassword: this.state.password
    }

    Api.post(API_URL.userInfo.USER_LOGIN, users).then((data: any) => {
      this.props.loginInfo(data);
      localStorage.setItem('userData', data.userId);
      localStorage.setItem('authToken', data.authToken);
      Api.setHeaders(data)
      this.props.history.push('/auth')
    });

    event.preventDefault();
  }
  public siginUp = ()=>{
    this.props.history.push('/signup')    
  }

  public render() {
    return (
      <div className="login-container">
        <div className="container ">
          <div className="shadow-lg bg-white rounded">
            <div className="row">
              <div className="col-md-5 content-col">
                <h2> Welcome Back! </h2>
                <div className="quote"> The keep connected with us please login with your personal info</div>
                <div className="signup-btn" onClick = {this.siginUp}>Sign Up</div>
              </div>
              <div className="col-md-7 form-col">
                <h1>Sign In into ...</h1>
                <form onSubmit={this.handleSubmit}>
                  <FormGroup  >
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <img width="36" height="18" src="/images/email.svg" alt="" />
                      </InputGroupAddon>
                      <Input id="email"
                        autoFocus={true}
                        type="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        placeholder="Email" />
                    </InputGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <img width="36" height="18" src="/images/lock.svg" alt="" />
                      </InputGroupAddon>
                      <Input id="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                        placeholder="Password"
                      />
                    </InputGroup>
                  </FormGroup>
                    <div className="signup-btn" onClick={this.handleSubmit} >Sign IN</div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  login: state.postsData.items
})
export default connect(mapStateToProps, { loginInfo })(Login);

