// import Users from '../../users/Users';
import Posts from '../../posts/posts';
import ChatComponent from 'src/components/chat/ChatComponent';
import Settings from 'src/components/settings/settings';

export const routes = [{
    path: "/chat",
    component: ChatComponent,
    icon:'conversation',
    exact:true
  },{
    path: "/settings",
    component: Settings,
    icon:'settings',
    exact:true
  },
  {
    path: "/posts",
    component: Posts,
    icon:'businessman',
    routes: [
      {
        path: "/user/posts",
        component: Posts
      },
      {
        path: "/post/user",
        component: Posts
      }
    ]
  }
];