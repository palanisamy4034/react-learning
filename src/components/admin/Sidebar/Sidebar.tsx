import * as React from 'react';
import './Sidebar.css';
import { Link } from "react-router-dom";
import { routes } from '../router/routes'

class SideBar extends React.Component<any, any>{
  constructor(props: any) {
    super(props);
  }
  public renderlink(routelink: any) {
    const path = window.location.pathname;
    return routelink.map((r: any, i: any) => {
      return (<Link to={r.path} key={i}>
        <div className={path === r.path ? "nav-item active ": "nav-item"} >
          {/* <i className="material-icons largeIcon">
            {r.icon}
          </i> */}
          <img width="36" src={'images/'+r.icon+'.svg'} alt="" />
        </div>
      </Link>
      )
    });
  }
  public render() {
    return (
      <ul>
        <div className="bar">
          <div className="nav-container">
            {this.renderlink(routes)}
          </div>
        </div>
      </ul>);
  }
}
export default SideBar;
