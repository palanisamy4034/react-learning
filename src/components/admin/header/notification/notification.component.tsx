

import * as React from 'react';
import './notification.scss';

class NotificationComponent extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
    }

    public render() {
        return (<div className="notification-wrapper">
            <div className="dropdowns-wrapper">
                <div className="dropdown-container">
                    <div className="notifications dropdown dd-trigger">
                        <span className="count animated" id="notifications-count">5</span>
                        <span className="fa fa-bell-o" />
                    </div>
                    <div className="dropdown-menu animated" id="notification-dropdown">
                        <div className="dropdown-header">
                            <span className="triangle" />
                            <span className="heading">Notifications</span>
                            <span className="count" id="dd-notifications-count"> 5 </span>
                        </div>
                        <div className="dropdown-body">
                            <div className="notification new">
                                <div className="notification-image-wrapper">
                                    <div className="notification-image">
                                        <img src="..." alt="" width="32" />
                                    </div>
                                </div>
                                <div className="notification-text">
                                    <span className="highlight">palani </span>  welcome
                  </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="dropdown-container">
                    <div className="messages dropdown">
                        <span className="fa fa-envelope-o" />
                    </div>
                </div>
            </div>
        </div>)
    }
}
export default NotificationComponent;