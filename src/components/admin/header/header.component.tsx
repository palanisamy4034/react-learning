import * as React from 'react';
import './header.scss';
// import HeaderLinks from './headerLinks';
import history from './../../../_helpers/history';

import { Collapse, Navbar, Nav, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { connect } from 'react-redux';
import NotificationComponent from './notification/notification.component';

class HeaderComponent extends React.Component<any, any>{
  constructor(props: any) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.logOutUser = this.logOutUser.bind(this);
    this.gotoSettings = this.gotoSettings.bind(this);
    this.state = {
      isOpen: false
    };
  }
  public toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  public logOutUser() {
    localStorage.clear();
    window.location.href = '/login'
  }
  public gotoSettings() {
    history.push('/settings')
  }

  public render() {
    return (
      <div>
        <Navbar color="light" light={true} expand="md">
          <img width="44px" src="images/logo-1.svg" alt="" />
          <Collapse isOpen={this.state.isOpen} navbar={true}>
            <Nav className="ml-auto" navbar={true}>
              {/* <NavItem componentClass='span'>  */}
              <NotificationComponent />
              {/* </NavItem> */}
              {/* <NavItem > */}
              <UncontrolledDropdown nav={true} inNavbar={true}>
                <DropdownToggle nav={true} caret={true}>
                  <div>
                    <img src="images/man.svg" alt="..." className="img-thumbnail" />
                    <div> {this.props.user.userName}</div>
                  </div>
                </DropdownToggle>
                <DropdownMenu right={true}>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    <div onClick={this.gotoSettings}> Settings </div>
                  </DropdownItem>
                  <DropdownItem divider={true} />
                  <DropdownItem>
                    <div onClick={this.logOutUser}>Logout</div>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              {/* </NavItem> */}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => ({
  user: state.loginData.loginInfo
})
export default connect(mapStateToProps)(HeaderComponent);
