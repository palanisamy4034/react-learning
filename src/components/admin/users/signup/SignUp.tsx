import * as React from 'react';
import {  FormGroup, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { connect } from 'react-redux';
import { loginInfo } from './../../../../store/actions/PostActions'
import Api from './../../../../config/Api';
import API_URL from './../../../../config/api.url';
import './../Users';

class SignUp extends React.Component<any, any>{
    public persons: any;

    constructor(props: any) {
        super(props);

        this.state = {
            email: "",
            password: "",
            username: "",
            mobile: ""
        };
    }
    // tslint:disable-next-line:no-empty
    public componentWillMount() {

    }
    public validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    public handleChange = (event: any) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    public handleSubmit = (event: any) => {
        const users = {
            userEmail: this.state.email,
            userPassword: this.state.password,
            userName: this.state.username,
            userMobile: this.state.mobile
        }
        Api.post(API_URL.userInfo.REGISTER_USER, users).then((data: any) => {
            this.props.history.push('/login')
        })
        event.preventDefault();
    }
    public siginIn = ()=>{
      this.props.history.push('/login')    
    }
    public render() {
        return (
            <div className="login-container">
            <div className="container ">
              <div className="shadow-lg bg-white rounded">
                <div className="row">
                  <div className="col-md-7 form-col">
                    <h1>Create a Account</h1>
                    <form>
                      <FormGroup  >
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <img width="36" height="18" src="/images/user.svg" alt="" />
                          </InputGroupAddon>
                          <Input id="username"
                            autoFocus={true}
                            type="text"
                            value={this.state.username}
                            onChange={this.handleChange}
                            placeholder="User Name" />
                        </InputGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <img width="36" height="18" src="/images/email.svg" alt="" />
                          </InputGroupAddon>
                          <Input id="email"
                            autoFocus={true}
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                            placeholder="Email" />
                        </InputGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <img width="36" height="18" src="/images/mobile.svg" alt="" />
                          </InputGroupAddon>
                          <Input id="mobile"
                            autoFocus={true}
                            type="text"
                            value={this.state.mobile}
                            onChange={this.handleChange}
                            placeholder="Mobile" />
                        </InputGroup>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <img width="36" height="18" src="/images/lock.svg" alt="" />
                          </InputGroupAddon>
                          <Input id="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                            placeholder="Password"
                          />
                        </InputGroup>
                      </FormGroup>
                        <div className="signup-btn" onClick={this.handleSubmit} >Sign UP</div>
                    </form>
                  </div>
                  <div className="col-md-5 content-col">
                    <h2> Hello Friend welcome Back! </h2>
                    <div className="quote"> The keep connected with us please login with your personal info</div>
                    <div className="signup-btn" onClick = {this.siginIn} >Sign IN</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    login: state.postsData.items
})
export default connect(mapStateToProps, { loginInfo })(SignUp);

