import * as React from 'react';
import { Route } from "react-router-dom";
import { routes } from '../router/routes'

class RouteComponent extends React.Component<any, any>{


  constructor(props: any) {
    super(props);
  }
  public RouteWithSubRoutes(route: any) {
    return (
      <Route exact={true}
        path={route.path} 
        // tslint:disable-next-line:jsx-no-lambda
        render={props => (
          <route.component {...props} routes={route.routes} />
        )}
      />
    );
  }
 
  public render() {

    return (<div className="app-container">{routes.map((route, i) => (
        <this.RouteWithSubRoutes key={i} {...route} />
      ))}
    </div>);
  }
}
export default RouteComponent;