import * as React from 'react';
// import API from './../../../config/Api'
// import Url from './../../../config/api.url'

import { connect } from 'react-redux';
import { fetchPosts, loginInfo } from './../../store/actions/PostActions'
import './posts.css'


class Posts extends React.Component<any, any>{
  public persons: any;
  constructor(props: any) {
    super(props);
  }
  public componentWillMount() {
    this.props.fetchPosts();
  }


  public render() {
    const postItems = this.props.posts.map((post: any) => (
      <div className="col-sm" key={post.id}>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">{post.title}</h5>
            <p className="card-text">{post.body}</p>
          </div>
      </div>
      </div>
    ))
    return (
      <div>
        <div>{this.props.user.email}</div>
        <div className="container">
        <div className="row">
            {postItems}
            </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => ({
  posts: state.postsData.posts,
  user: state.loginData.loginInfo
})
export default connect(mapStateToProps, { fetchPosts, loginInfo })(Posts);
