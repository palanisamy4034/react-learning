import * as React from 'react';
import './popup.component.scss'
import { enablePopupModal } from 'src/store/actions/loaderActions';
import { connect } from 'react-redux';
import Button from 'reactstrap/lib/Button';

class PopupWindowComponent extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = { title: this.props.title }
        this.closeModal = this.closeModal.bind(this)
    }
    public closeModal = () => {
        this.props.enablePopupModal({isOpen:false})
    }

    public render() {
        return (<section className="common-popupmodal">
            <div className="popup-header">
                <div className="popupmodal">
                    <div className="col-lg"> hai </div>
                    <Button color="primary" onClick={this.closeModal}> close  </Button>
                </div>
            </div>
        </section>);
    }
}
const mapStateToProps = (state: any) => ({
    user: state.loaderStatus.popupIsOpen
})

export default connect(mapStateToProps, { enablePopupModal })(PopupWindowComponent);
