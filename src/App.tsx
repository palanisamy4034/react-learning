import * as React from 'react';
import { Router, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import './App.scss';
import Login from './components/admin/users/login/Login';
import SignUp from './components/admin/users/signup/SignUp';
import SideBar from './components/admin/Sidebar/Sidebar';
import RouteComponent from './components/admin/routecomponent/routeComponent';
import store from './store/Store';
import { connect } from 'react-redux';
import { loginInfo } from './store/actions/PostActions';
import history from './_helpers/history';
import { updateCoversationMessages, initCoversationMessages } from './store/actions/chatActions';
import Authentication from './components/auth/authComponent'
import SocketService from './socket/socketConnection';
import { cloneDeep } from 'lodash';
import HeaderComponent from './components/admin/header/header.component';
import LoaderComponent from './components/loader/loader.component';
import PopupWindowComponent from './components/popupwindow/popupwindow';

class App extends React.Component<any, any> {
  
  public state = {
    isAuth: false, error: null, errorInfo: null
  };

  constructor(props: any) {
    super(props);
    this.unsubscribe();
  }

  public unsubscribe = () => store.subscribe(this.handleChange.bind(this, true))
  
  public handleChange(eve: any) {
    const initStoreData = store.getState();
    const currentState = initStoreData.loginData.isLoggedIn;
    if (currentState !== this.state.isAuth && currentState === true &&
      initStoreData.loginData.loginInfo !== undefined) {
      this.setState({ isAuth: true });
      this.enablesocketConn(cloneDeep(initStoreData.loginData));
    }
  }

  public enablesocketConn(userInfo: any) {
    const socket = SocketService.enableIOConnection(userInfo.loginInfo.userId);
    socket.on('messagerecive', (response: any) => {
      this.props.updateCoversationMessages(response);
    });
  }

  public componentDidCatch(error:any, errorInfo:any) {
    // Catch errors in any components below and re-render with error message
    this.setState({error,errorInfo});
    // tslint:disable-next-line:no-console
    console.log(error)
  }

  public render() {
    const isLoggedIn = () => {
      return this.props.user.isLoggedIn;
    }
    const currentRoute = () => {
      const location = window.location.pathname
      const searchParam = window.location.search;
      if (location === '/login') {
        history.push(`${location}${searchParam}`);
      } else if (location === '/signup') {
        history.push(`${location}${searchParam}`);
      } else if (location === '/auth' && searchParam !== '') {
        history.push(`${location}${searchParam}`);
      } else if (location === '/') {
        history.push(`/auth${searchParam}`);
      } else {
        history.push(`/auth${searchParam}`);
      }
    }

    if (isLoggedIn() === false) {
      currentRoute();
    }

    return (
      <div className="main">
        <div className="App">
          <div>
            <Router history={history}>
              <div>
                {isLoggedIn() === true && <div>
                  <div className="header-component"> <HeaderComponent /></div>
                  <div className="sidebar">
                    <SideBar />
                  </div>
                  <div className="container-component">
                    <RouteComponent />
                  </div>
                </div>}
                {/* <Redirect to='/'/> */}
                <Route path='/auth' component={Authentication} />
                <Route path='/login' exact={true} component={Login} />
                <Route path='/signup' exact={true} component={SignUp} />
              </div>
            </Router>
          </div>
        </div>
        <ToastContainer position="top-right" autoClose={3000} hideProgressBar={true} newestOnTop={true} closeOnClick={true} rtl={false} draggable={true} pauseOnHover={true} />
        <div>
          {this.props.loader === true && <LoaderComponent />}
        </div> 
        <div>
          {this.props.popupshow === true && <PopupWindowComponent />}
        </div>
      </div>);
  }
}

const mapStateToProps = (state: any) => ({
  user: state.loginData,
  popupshow: state.loaderStatus.popupIsOpen,
  loader: state.loaderStatus.loaderIsOpen
})

export default connect(mapStateToProps, { loginInfo, updateCoversationMessages, initCoversationMessages })(App);
